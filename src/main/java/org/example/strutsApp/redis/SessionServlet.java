package org.example.strutsApp.redis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.strutsApp.LoginAction;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


// tag::class[]
public class SessionServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(LoginAction.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String attributeName = req.getParameter("attributeName");
        String attributeValue = req.getParameter("attributeValue");
        req.getSession().setAttribute(attributeName, attributeValue);
        resp.sendRedirect(req.getContextPath() + "/");
        LOGGER.info("\n******** USER POST REQUEST -> {}",req.getParameter("attributeName"));
    }

    private static final long serialVersionUID = 2878267318695777395L;

}

// end::class[]

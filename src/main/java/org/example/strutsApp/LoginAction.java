package org.example.strutsApp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends Action {

    private static final Logger LOGGER = LogManager.getLogger(LoginAction.class);

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser (User user) {
        this.user = user;
    }


    public ActionForward execute(ActionMapping mapping, ActionForm form,
                                 HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        LOGGER.info("\n\n******** USER ATTEMPTING TO LOG IN, in LoginAction.class*******\n\n");

        String target = "";

        user = new User();

        target = user.validateUser("admin",
                "123");

        return mapping.findForward(target);
    }
}

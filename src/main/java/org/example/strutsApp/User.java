package org.example.strutsApp;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class User {
    private static final Logger LOGGER = LogManager.getLogger(User.class);

    public String validateUser(String userName, String password) {
        if(userName.equals("admin") && password.equals("123")) {
            LOGGER.info("\n\n******** VALIDATING in User.class *******\n\n");
            return "success";
        }

        return "failure";
    }
}

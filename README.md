# Spring 3 and Struts 1.x Webapp

This repository contains the source code for a Java webapp that utlizies Springweb 3.x and Struts 1.3


### Build

You can create the .war file by running the following maven command:
```
mvn clean install
```

The resulting file can be found in the `/target` folder


### Deploying on tomcat

When deploying this webapp to tomcat, you can reach the welcome page by placing the war files name after your hostname in the URL
example:
```
http://hostname.com/struts-spring3/
```

This will render the `welcome.jsp` file found in the webapp folder.
# Redis
This project uses redis to back the spring sessions. To utulize this, please set the environemnt variables
for your connection details

```
export REDIS_HOST=localhost
export REDIS_PORT=6379
export REDIS_USE_POOL=true
export REDIS_DATABASE=0
export REDIS_PASSWORD= 
```

# Endpoints

|Request | Endpoint|
|-------|------------|
|GET|`/struts-spring3/login.do`|
|GET|`/struts-spring3/index.jsp`|
